import os
import numpy as np
import pandas as pd
import scipy.stats
import matplotlib.pyplot as plt

col_list_player = ["Player", "Pos", "GP", "G", "PTS"]
col_list_team = ["Team", "W", "PTS", "PTS%"]
starting_path = "..\\etc\\player_data\\19-20\\"
team_path = "..\\etc\\league_data\\Teams_19-20.csv"
main_var = "PTS"
y_metric = "PTS%"

team_names = []
t6_values = []
b6_values = []
d6_values = []

def create_data_frames(name_list, value_list, distigusing_char):
    temp = {"Team": name_list, distigusing_char: value_list}
    return pd.DataFrame(temp, columns = ["Team", distigusing_char]).sort_values(["Team"], ascending = True).set_index("Team")
    
def update_player_ranking(data_frame, i, list, highest=True):
    if(highest and (data_frame.iloc[i, col_list_player.index(main_var)] > list[i][0])) or (not highest and (data_frame.iloc[i, col_list_player.index(main_var)] < list[i][0])):
        list[i][0] = data_frame.iloc[i, col_list_player.index(main_var)]
        list[i][1] = data_frame.iloc[i, col_list_player.index("Player")]
        
def display_best_team_sum(team_list, x6d):
    ret_val = pd.DataFrame()
    for teami in team_list.index:
        ret_val = ret_val.append(x6d[teami][main_var])
    
    for (columni_name, columni_data) in ret_val.iteritems():
            print(columni_data.sum()/len(ret_val))    
    
    return ret_val
    
def plot_with_corr(data_frame, x_metric, y_metric):
    slope, intercept, r, p, stderr = scipy.stats.linregress(data_frame[x_metric], data_frame[y_metric])
    line = f"Regression line: {y_metric}={intercept:.5f}+{slope:.5f}{x_metric}, r={r:.5f}, p={p:.5f}"
    fig, ax = plt.subplots()
    ax.plot(data_frame[x_metric], data_frame[y_metric], linewidth=0, marker='s', label='Data points')
    ax.plot(data_frame[x_metric], intercept + slope * data_frame[x_metric], label=line)
    ax.set_xlabel(x_metric)
    ax.set_ylabel(y_metric)
    ax.legend(facecolor="white")

if __name__ == "__main__":
    '''
    Create Variables
    '''
    # Dataframe storage
    t6d = {}
    b6d = {}
    d6d = {}
    
    # Player Lists (x -> max n -> min)
    t6x = [[0, ""], [0, ""], [0, ""], [0, ""], [0, ""], [0, ""]]
    t6n = [[np.inf, ""], [np.inf, ""], [np.inf, ""], [np.inf, ""], [np.inf, ""], [np.inf, ""]]
    b6x = [[0, ""], [0, ""], [0, ""], [0, ""], [0, ""], [0, ""]]
    b6n = [[np.inf, ""], [np.inf, ""], [np.inf, ""], [np.inf, ""], [np.inf, ""], [np.inf, ""]]
    d6x = [[0, ""], [0, ""], [0, ""], [0, ""], [0, ""], [0, ""]]
    d6n = [[np.inf, ""], [np.inf, ""], [np.inf, ""], [np.inf, ""], [np.inf, ""], [np.inf, ""]]
    
    '''
    Go through the csvs
    '''
    year = os.listdir(starting_path)
    for file in year:
        # Clean the team name
        team_i = file.split("_")[0].replace("-", " ")
        team_names.append(team_i)
        
        # Get the data from team i
        dfi = pd.read_csv(starting_path+file, header=1)
        
        # Remove the not needed data
        dfi = dfi[dfi.Player != "Team Total"]
        dfi = dfi[col_list_player]
        dfi = dfi.sort_values([main_var, "GP", "Player"], ascending = [False, True, True])
        
        # Create the Defence 6
        dfid = dfi[dfi["Pos"] == "D"].head(n=6).reset_index(drop=True)
        
        # Create the Top 6 and Bottom 6
        dfif = dfi[dfi["Pos"] != "D"]
        dfift = dfif[0:6].reset_index(drop=True)
        dfifb = dfif[6:12].reset_index(drop=True)
        
        # Store the X6's 
        t6d[team_i] = dfift
        b6d[team_i] = dfifb
        d6d[team_i] = dfid
        
        # Update the Player Lists
        for i in range(6):
            update_player_ranking(dfift, i, t6x, True)
            update_player_ranking(dfift, i, t6n, False)
            update_player_ranking(dfifb, i, b6x, True)
            update_player_ranking(dfifb, i, b6n, False)
            update_player_ranking(dfid, i, d6x, True)
            update_player_ranking(dfid, i, d6n, False)
        
        # Get and Store the sums
        t6_values.append(dfift[main_var].sum())
        b6_values.append(dfifb[main_var].sum())
        d6_values.append(dfid[main_var].sum())
    
    '''
    Print the best 5 teams in each category
    '''
    print("Top 5 Teams in Each Category")
    dft6 = create_data_frames(team_names, t6_values, "T6")
    dfb6 = create_data_frames(team_names, b6_values, "B6")
    dfd6 = create_data_frames(team_names, d6_values, "D6")
    
    dft6t5 = dft6.sort_values("T6", ascending = False).head(n=5)
    dfb6t5 = dfb6.sort_values("B6", ascending = False).head(n=5)
    dfd6t5 = dfd6.sort_values("D6", ascending = False).head(n=5)
    
    print(dft6t5)
    print(dfb6t5)
    print(dfd6t5)
    print("")
    
    print("Bottom 5 Teams in Each Category")
    print(dft6.sort_values("T6", ascending = True).head(n=5))
    print(dfb6.sort_values("B6", ascending = True).head(n=5))
    print(dfd6.sort_values("D6", ascending = True).head(n=5))
    print("")
    
    '''
    Print the Player Lists
    '''
    print("Top/Bottom Players")
    print(t6x)
    print(t6n)
    print(b6x)
    print(b6n)
    print(d6x)
    print(d6n)
    print("")
    
    '''
    Find and Print average values
    '''
    # Top 5
    print("Top 5 Team's Players Averaged")
    display_best_team_sum(dft6t5, t6d)
    display_best_team_sum(dfb6t5, b6d)
    display_best_team_sum(dfd6t5, d6d)
    print("")
    
    # All
    print("All Team's Players Averaged")
    display_best_team_sum(dft6.sort_values("T6", ascending = False), t6d)
    display_best_team_sum(dfb6.sort_values("B6", ascending = False), b6d)
    display_best_team_sum(dfd6.sort_values("D6", ascending = False), d6d)
    print("")
    
    '''
    Find and Plot the r values
    '''
    print("r Values")
    dft = pd.read_csv(team_path, header=1)
    dft = dft[col_list_team]
    dft = dft[dft.Team != "League Average"]
    dft = dft.sort_values("Team", ascending = True).assign(Team = team_names).set_index("Team")
    
    dff = dft.join(dft6, on="Team").join(dfb6, on="Team").join(dfd6, on="Team")
    dff = dff.sort_values([y_metric], ascending = False)
    
    print(scipy.stats.spearmanr(dff["T6"], dff[y_metric]))
    print(scipy.stats.spearmanr(dff["B6"], dff[y_metric]))
    print(scipy.stats.spearmanr(dff["D6"], dff[y_metric]))
    print(scipy.stats.kendalltau(dff["T6"], dff[y_metric]))
    print(scipy.stats.kendalltau(dff["B6"], dff[y_metric]))
    print(scipy.stats.kendalltau(dff["D6"], dff[y_metric]))
    
    plot_with_corr(dff, "T6", y_metric)
    plot_with_corr(dff, "B6", y_metric)
    plot_with_corr(dff, "D6", y_metric)
    
    plt.show()
    