# T6 B6 D6

How to run
----

```
This code is dependant on several libraries.  Luckily, all of these libraries can be installed using Anaconda.  
Go to https://www.anaconda.com/products/individual to download and install.  

Once Anaconda is installed:
Open an Anaconda Prompt,
cd to the directory you downloaded the code from, and 
type "python t6b6d6_processing.py"
```

Developer Notes
----

```
Developed using Anaconda3 2019.10 (Python 3.7.4 64-bit) 
```